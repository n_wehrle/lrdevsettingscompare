local DevelopmentBlocks = require 'DevelopmentBlocks'
local FilterUtil = {}

-----------------------------------------------------------------------
function FilterUtil.buildTables(_filterName)
  local filterTable = {}
  local triggerTable = {}

  for _,block in ipairs(DevelopmentBlocks) do
    for _,subBlock in ipairs(block) do
      for _,v in ipairs(subBlock) do
        if (_filterName == block.filterBy or _filterName == subBlock.filterBy) then
          table.insert(filterTable, v)
        end
        if (_filterName == block.triggerFilter or _filterName == subBlock.triggerFilter) then
          table.insert(triggerTable, v)
        end
      end
    end
  end

  return filterTable, triggerTable
end

-----------------------------------------------------------------------
return FilterUtil