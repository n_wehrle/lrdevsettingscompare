--global
local LrApplication = import 'LrApplication'
local LrTasks = import 'LrTasks'
local LrErrors = import 'LrErrors'
local LrFunctionContext = import 'LrFunctionContext'
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'

--local
local BackendUtil = require 'BackendUtil'
local FrontendUtil = require 'FrontendUtil'
local PluginProperties = require 'PluginProperties'
local PrefsUtil = require 'PrefsUtil'
local LogUtil = require 'LogUtil'
local PresetUtil = require 'PresetUtil'
local RefSettingsUtil = require 'RefSettingsUtil'
local DevelopmentBlocks = require 'DevelopmentBlocks'
local BWFilter = require 'BWFilter'
local PresetFilter = require 'PresetFilter'

--globals, todo: refactor to local
photoActive, photoOther = nil, nil


-- todo: caller must check return value ...
-- local retVal,errMsg = LrTasks.pcall(compareDevSettings, devSettingsActive, devSettingsOther)
-- ... and throw errors in one of the following flavours
-- error "Example of an internal error"
-- LrErrors.throwUserError( "Example of a localized error message." )
-- assert (returnvalue, "Example of an internal error")
local function compareDevSettings(devSettingsActive, devSettingsOther)
  local threshold = PrefsUtil.getThreshold()
  local debugMode = PrefsUtil.isDebugModeEnabled()

  -- iterate devsettings of first photo, compare with second, save delta in table
  local deltaBack = BackendUtil.flattenedDelta(devSettingsActive, devSettingsOther)

  -- iterate devsettings of second photo, compare with first, use the same table to save
  local deltaBackForth = BackendUtil.flattenedDelta(devSettingsOther, devSettingsActive, deltaBack, nil, true)

  -- todo: refactor to filter chain chunk
  deltaBackForth = BWFilter.filter(deltaBackForth)
  deltaBackForth = PresetFilter.filter(deltaBackForth)

  local panel = nil
  local blockCount, count = BackendUtil.countRelevantEntries(deltaBackForth, DevelopmentBlocks)

  -- decides if a compact table or a tabbedPane will be rendered
  if (count <= threshold) then
    panel = FrontendUtil.formOutputTable(
      deltaBackForth
      ,DevelopmentBlocks
    )
  else
    panel = FrontendUtil.formTabbedPane(
      deltaBackForth
      ,DevelopmentBlocks
      ,blockCount
    )
  end

  local f = LrView.osFactory()
  LrDialogs.presentModalDialog {
    title = LOC "$$$/lidiplu/Window/Main/Title=Development Settings Comparison",
    contents = panel,
    --[[
    accessoryView = f:view {
      spacing = 0,
      margin = 0,
      f:picture{
        value = _PLUGIN:resourceId("img/round_50.png"),
        tooltip = PluginProperties.Copyright,
      },
    },
    --]]
  }

  if debugMode then
    LogUtil.getDefaultLogger():trace("==========================")
    LogUtil.vardump(deltaBackForth)
  end
end

-- main
local function main(context)
  -- only needed if not called with LrTasks.startAsyncTask()
  LrDialogs.attachErrorDialogToFunctionContext(context)

  local catalog = LrApplication.activeCatalog()
  local selectedPhotos = catalog:getTargetPhotos()

  if (#selectedPhotos == 2) then
    photoActive = selectedPhotos[1]
    photoOther = selectedPhotos[2]

    -- active photo is always the first one
    if (catalog:getTargetPhoto() == photoOther) then
      photoActive, photoOther = photoOther, photoActive
    end

    local devSettingsActive = photoActive:getDevelopSettings()
    local devSettingsOther = photoOther:getDevelopSettings()

    assert (nil ~= devSettingsActive, "Devsettings for active photo must not be null")
    assert (nil ~= devSettingsOther,  "Devsettings for other photo must not be null")

    -- default: no special error handling 
    compareDevSettings (devSettingsActive, devSettingsOther)

    -- selection can be changed in comparison panel, so we must always switch back
    FrontendUtil.showActivePhotoInNavigator()

  elseif (#selectedPhotos == 1) then
    local refSettingsPreset = RefSettingsUtil.getRefSettingsPreset()
    -- Reference settings valid if not nil, and(!) there's a name in the preferences
    if (nil ~= refSettingsPreset and PrefsUtil.getRefSettingsMeta()) then
      photoActive = selectedPhotos[1]
      photoOther = nil

      local devSettingsActive = photoActive:getDevelopSettings()
      local devSettingsPreset = refSettingsPreset:getSetting()

      assert (nil ~= devSettingsActive, "Devsettings for active photo must not be null")
      assert (nil ~= devSettingsPreset, "Devsettings for preset must not be null")

      -- default: no special error handling
      compareDevSettings(devSettingsActive, devSettingsPreset)
    else
      -- warning: there is a maximum of chars(255?) and lines(3?) for a message
      local message = LOC ("$$$/lidiplu/Warning/RefSettingsNotFound=No reference settings found.")
      LrDialogs.message(message, nil, "warning")
    end
  else
    local message = LOC "$$$/lidiplu/Warning/Selection=Please select exactly one or two photos."
    LrDialogs.message(message, nil, "info")
  end
end

-- async (required by some functions in the method body)
-- withContext (needed for error handling)
LrFunctionContext.postAsyncTaskWithContext("LrDevSettingsCompareAsync", main)

-- async (required by some functions in the method body)
-- no context, but default errorhandling by dialogs activated
-- LrTasks.startAsyncTask(main)