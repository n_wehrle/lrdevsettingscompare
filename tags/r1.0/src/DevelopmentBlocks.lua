return {

  -- headline matches the name of a development panel in LR3
  -- keys match the dev-settings used in that panel
  -- retouching not supported yet

  {
    title = "$$$/AgDevelop/Crop/Crop&Straighten",
    filterBy = "PresetFilter",
    {
      "CropTop",             -- number
      "CropRight",           -- number
      "CropBottom",          -- number
      "CropLeft",            -- number
      "CropAngle",           -- number
      "CropConstrainToWarp", -- number (1 = true?)
      --"Orientation",       -- string (unknown if right place here)
      --"CropConstrainAspectRatio", -- seems to be always empty
    },
  },

  { -- Basic
    title = "$$$/AgDevelop/Panel/BasicAdjustments",
    -- todo: Treatment: Color | Black & White ?

    {
      "WhiteBalance",      -- string
      "Temperature",       -- number
      "Tint",              -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Basic/Tone",
      "Exposure",          -- number
      "HighlightRecovery", -- number
      "FillLight",         -- number
      "Shadows",           -- number
    },
    {
      "Brightness",        -- number
      "Contrast",          -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Basic/Presence",
      "Clarity",           -- number
      "Vibrance",          -- number
      "Saturation",        -- number
    },
  },
  { -- Tone Curve
    title = "$$$/AgDevelop/Panel/ToneCurve",
    {
      summary = {
        title="$$$/AgDevelop/Panel/ToneCurve",
        changed="$$$/lidiplu/Summary/ToneCurve/Changed",
        unchanged="$$$/lidiplu/Summary/ToneCurve/Unchanged",
      },
      "ToneCurve.1",
      "ToneCurve.2",
      "ToneCurve.3",
      "ToneCurve.4",
      "ToneCurve.5",
      "ToneCurve.6",
      "ToneCurve.7",
      "ToneCurve.8",
      "ToneCurve.9",
      "ToneCurve.10",
      "ToneCurve.11",
      "ToneCurve.12",
      "ToneCurve.13",
      "ToneCurve.14",
      "ToneCurve.15",
      "ToneCurve.16",
      "ToneCurve.17",
      "ToneCurve.18",
      "ToneCurve.19",
      "ToneCurve.20",
      "ToneCurve.21",
      "ToneCurve.22",
      "ToneCurve.23",
      "ToneCurve.24",
      "ToneCurve.25",
      "ToneCurve.26",
      "ToneCurve.27",
      "ToneCurve.28",
      "ToneCurve.29",
      "ToneCurve.30",
      "ToneCurve.31",
      "ToneCurve.32",
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/ToneCurve/Region",
      "ParametricHighlights",     -- number
      "ParametricLights",         -- number
      "ParametricDarks",          -- number
      "ParametricShadows",        -- number
      -- Dividers below curve
      "ParametricShadowSplit",    -- number
      "ParametricMidtoneSplit",   -- number
      "ParametricHighlightSplit", -- number
    },
    {
      -- Point Curve
      "ToneCurveName",            -- string
    }
  },

  {
    title = "$$$/AgDevelop/ColorAdjustments/WhichSliders/ByHSL",
    filterBy = "BWFilter",
    { -- HSL / Color / B & W -> HSL -> Hue
      title = "$$$/AgCameraRawUI/HSLAdjustment/Hue",
      "HueAdjustmentRed",            -- number
      "HueAdjustmentOrange",         -- number
      "HueAdjustmentYellow",         -- number
      "HueAdjustmentGreen",          -- number
      "HueAdjustmentAqua",           -- number
      "HueAdjustmentBlue",           -- number
      "HueAdjustmentPurple",         -- number
      "HueAdjustmentMagenta",        -- number
    },

    { -- HSL / Color / B & W -> HSL -> Saturation
      title = "$$$/AgCameraRawUI/HSLAdjustment/Saturation",
      "SaturationAdjustmentRed",     -- number
      "SaturationAdjustmentOrange",  -- number
      "SaturationAdjustmentYellow",  -- number
      "SaturationAdjustmentGreen",   -- number
      "SaturationAdjustmentAqua",    -- number
      "SaturationAdjustmentBlue",    -- number
      "SaturationAdjustmentPurple",  -- number
      "SaturationAdjustmentMagenta", -- number
    },

    { -- HSL / Color / B & W -> HSL -> Luminance
      title = "$$$/AgCameraRawUI/HSLAdjustment/Luminance",
      "LuminanceAdjustmentRed",      -- number
      "LuminanceAdjustmentOrange",   -- number
      "LuminanceAdjustmentYellow",   -- number
      "LuminanceAdjustmentGreen",    -- number
      "LuminanceAdjustmentAqua",     -- number
      "LuminanceAdjustmentBlue",     -- number
      "LuminanceAdjustmentPurple",   -- number
      "LuminanceAdjustmentMagenta",  -- number
    },
  },

  {
    title = "$$$/AgDevelop/ColorAdjustments/WhichSliders/ByColor",
    filterBy = "BWFilter",
    { -- HSL / Color / B & W -> Color -> Red
      title = "$$$/AgCameraRawUI/HSLAdjustment/Red",
      "HueAdjustmentRed", -- number
      "SaturationAdjustmentRed", -- number
      "LuminanceAdjustmentRed", -- number
    },

    { -- HSL / Color / B & W -> Color -> Orange
      title = "$$$/AgCameraRawUI/HSLAdjustment/Orange",
      "HueAdjustmentOrange", -- number
      "SaturationAdjustmentOrange", -- number
      "LuminanceAdjustmentOrange", -- number
    },

    { -- HSL / Color / B & W -> Color -> Yellow
      title = "$$$/AgCameraRawUI/HSLAdjustment/Yellow",
      "HueAdjustmentYellow", -- number
      "SaturationAdjustmentYellow", -- number
      "LuminanceAdjustmentYellow", -- number
    },

    { -- HSL / Color / B & W -> Color -> Green
      title = "$$$/AgCameraRawUI/HSLAdjustment/Green",
      "HueAdjustmentGreen", -- number
      "SaturationAdjustmentGreen", -- number
      "LuminanceAdjustmentGreen", -- number
    },

    { -- HSL / Color / B & W -> Color -> Aqua
      title = "$$$/AgCameraRawUI/HSLAdjustment/Aqua",
      "HueAdjustmentAqua", -- number
      "SaturationAdjustmentAqua", -- number
      "LuminanceAdjustmentAqua", -- number
    },

    { -- HSL / Color / B & W -> Color -> Blue
      title = "$$$/AgCameraRawUI/HSLAdjustment/Blue",
      "HueAdjustmentBlue", -- number
      "SaturationAdjustmentBlue", -- number
      "LuminanceAdjustmentBlue", -- number
    },

    { -- HSL / Color / B & W -> Color -> Purple
      title = "$$$/AgCameraRawUI/HSLAdjustment/Purple",
      "HueAdjustmentPurple", -- number
      "SaturationAdjustmentPurple", -- number
      "LuminanceAdjustmentPurple", -- number
    },

    { -- HSL / Color / B & W -> Color -> Magenta
      title = "$$$/AgCameraRawUI/HSLAdjustment/Magenta",
      "HueAdjustmentMagenta", -- number
      "SaturationAdjustmentMagenta", -- number
      "LuminanceAdjustmentMagenta", -- number
    },
  },

  {
    title = "$$$/AgDevelop/ColorAdjustments/Grayscale",
    triggerFilter = "BWFilter",
    { -- HSL / Color / B & W -> B & W
      title = "$$$/AgDevelop/CameraRawPanel/Mixer/Grayscale/GrayscaleMix",
      "GrayMixerRed",     -- number
      "GrayMixerOrange",  -- number
      "GrayMixerYellow",  -- number
      "GrayMixerGreen",   -- number
      "GrayMixerAqua",    -- number
      "GrayMixerBlue",    -- number
      "GrayMixerPurple",  -- number
      "GrayMixerMagenta", -- number
    },
  },

  {
    title = "$$$/AgDevelop/Panel/SplitToning",
    {
      title = "$$$/AgDevelop/CameraRawPanel/Highlights",
      "SplitToningHighlightHue",        -- number
      "SplitToningHighlightSaturation", -- number
    },
    {
      "SplitToningBalance",             -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Shadows",
      "SplitToningShadowHue",           -- number
      "SplitToningShadowSaturation",    -- number
    },
  },

  { -- Detail
    title = "$$$/AgDevelop/Panel/Detail",
    {
      title = "$$$/AgDevelop/CameraRawPanel/Detail/Sharpening",
      "Sharpness",                       -- number
      "SharpenRadius",                   -- number
      "SharpenDetail",                   -- number
      "SharpenEdgeMasking",              -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Detail/NoiseReduction",
      "LuminanceSmoothing",              -- number
      "LuminanceNoiseReductionDetail",   -- number
      "LuminanceNoiseReductionContrast", -- number
    },
    {
      "ColorNoiseReduction",             -- number
      "ColorNoiseReductionDetail",       -- number
    },
  },


  {
    title = "$$$/AgDevelop/Panel/LensCorrections",
    {
      "LensProfileSetup",                -- string
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/LensCorrection/LensProfile",
      "LensProfileName",                 -- string
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/LensCorrection/Amount",
      "LensProfileDistortionScale",          -- number
      "LensProfileChromaticAberrationScale", -- number
      "LensProfileVignettingScale",          -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Transform",
      "LensManualDistortionAmount",      -- number
      --todo: switched in portrait-mode (maybe only if autorotated)
      "PerspectiveVertical",             -- number
      "PerspectiveHorizontal",           -- number
      "PerspectiveRotate",               -- number
      "PerspectiveScale",                -- number
      "CropConstrainToWarp",             -- number (1 = true?)
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Vignettes/LensCorrection",
      "VignetteAmount",       -- number
      "VignetteMidpoint",     -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/ChromaticAberration",
      "ChromaticAberrationR", -- number
      "ChromaticAberrationB", -- number
      "Defringe",             -- number (Off|Highlight Edges|All Edges)
    },
  },

  {
    title = "$$$/AgDevelop/Panel/Effects",
    {
      title = "$$$/AgDevelop/CameraRawPanel/Effects/PostCrop",
      "PostCropVignetteStyle",     -- number (Highl.Prio.|Col.Prio.|Paint Overl.)
      "PostCropVignetteAmount",    -- number
      "PostCropVignetteMidpoint",  -- number
      "PostCropVignetteRoundness", -- number
      "PostCropVignetteFeather",   -- number
      "PostCropVignetteHighlightContrast", -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Detail/Grain",
      "GrainAmount",               -- number
      "GrainSize",                 -- number
      "GrainFrequency",            -- number (Roughness)
    },
  },

  {
    title = "$$$/AgDevelop/Panel/Calibration",
    {
      "ProcessVersion",  -- string (5.0=2003|5.7=2010)
    },
    {
      "CameraProfile",   -- string
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/Shadows",
      "ShadowTint",      -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/RedPrimary",
      "RedHue",          -- number
      "RedSaturation",   -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/GreenPrimary",
      "GreenHue",        -- number
      "GreenSaturation", -- number
    },
    {
      title = "$$$/AgDevelop/CameraRawPanel/BluePrimary",
      "BlueHue",         -- number
      "BlueSaturation",  -- number
    },
  },

--[[
  { -- Misc (for testing)
    title = "Misc",
    "AutoBrightness",                 -- boolean
    "AutoContrast",                   -- boolean
    "AutoExposure",                   -- boolean
    "AutoShadows",                    -- boolean
    "ConvertToGrayscale",             -- boolean
    "EnableCalibration",              -- boolean
    "EnableLensCorrections",          -- boolean
    "EnableColorAdjustments",         -- boolean
    "EnableDetail",                   -- boolean
    "EnableEffects",                  -- boolean
    "EnableGradientBasedCorrections", -- boolean
    "EnableGrayscaleMix",             -- boolean
    "EnablePaintBasedCorrections",    -- boolean
    "EnableRedEye",                   -- boolean
    "EnableRetouch",                  -- boolean
    "EnableSplitToning",              -- boolean
  },
--]]
}
