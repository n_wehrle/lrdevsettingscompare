--global
local LrTasks = import 'LrTasks'
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'

--local
local PresetUtil = require 'PresetUtil'

local PresetChooserDialog = {}

function PresetChooserDialog.choose()
  local f = LrView.osFactory()

  local allNames, allUuids = PresetUtil.getAllPresets()
  local presetSelection = f:edit_field {
    auto_completion = true,
    completion = allNames,
    value = nil,
    validate = function(view, value)
      if (nil == value or "" == value or PresetUtil.presetForName(value)) then
        return true, value
      else
        return false, nil, LOC("$$$/lidiplu/Warning/NoPreset=^{^1^} is no valid preset", value)
      end
    end
  }

  if ("ok" == LrDialogs.presentModalDialog{
    title = LOC "$$$/lidiplu/Window/Preset/Title=Choose Preset",
    contents = presetSelection,
    resizable = true,
  }) then
    return presetSelection.value
  end

  return nil
end

return PresetChooserDialog