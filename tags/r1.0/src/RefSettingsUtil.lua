local LrApplication = import "LrApplication"
local PresetUtil = require 'PresetUtil'

local RefSettingsUtil = {}
local refSettingsPresetName = "lidiplu_ref_settings_preset"


function RefSettingsUtil.getRefSettingsPreset()
  local presets = LrApplication.getDevelopPresetsForPlugin(_PLUGIN)
  for _, preset in ipairs(presets) do
    if (refSettingsPresetName == preset:getName()) then
      return preset
    end
  end
  return nil
end

function RefSettingsUtil.setRefSettingsFromPreset(presetName)
  assert(nil ~= presetName, "Preset name must not be nil")

  local preset = PresetUtil.presetForName(presetName)
  if (preset) then
    LrApplication.addDevelopPresetForPlugin(
      _PLUGIN,
      refSettingsPresetName,
      preset:getSetting()
  )
  end

  -- needed to show in pluginMan and in comparison view
  -- first return value is reference type, can be looked up in PluginProperties
  return 1, preset:getName(), nil
end

function RefSettingsUtil.setRefSettingsFromSelectedPhoto(selPhoto)
  assert(nil ~= selPhoto, "Passed photo must not be nil")

  local settings = selPhoto:getDevelopSettings()
  LrApplication.addDevelopPresetForPlugin(_PLUGIN, refSettingsPresetName, settings)

  -- needed to show in pluginMan and in comparison view
  -- first return value is reference type, can be looked up in PluginProperties
  return 2, selPhoto:getFormattedMetadata("fileName"), selPhoto:getFormattedMetadata("copyName")
end

return RefSettingsUtil