--[[
  Compares the development settings of
  two different photos.

  Plugin:  LrDevSettingsCompare
  Version: 20101118(SVN 178M)
  Authors: Norman Wehrle, Matthias Saft

  Done:
    - compares two photos
    - compares a photo with a default preset
    - shows differences side by side
    - does not show similarities

  ======================================================
  
  Todo:
    - calc delta and render:
        Clarity  |  1  |  17(+16)
    - beautify GUI more
        color coding (small change, big change, value only here)
        sliders or charts instead of bare numbers
    - compare dev presets (2nd menu entry = 2nd script)
    - statistics?
    - apply presets in some way?
]]
return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 3.0,

	-- todo: identifier to be discussed
    LrToolkitIdentifier = 'ch.dslr.lightroom.lrdevsettingscompare',
    LrPluginName = LOC "$$$/lidiplu/PluginName=DevSettingsCompare",
    LrPluginInfoUrl = "http://sourceforge.net/projects/lidiplu/",
	
    LrPluginInfoProvider = "PluginInfoProvider.lua",

	-- only in file menu until yet
    LrExportMenuItems = {
      {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompare=Compare Development Settings",
		file = "PluginMain.lua",
        enabledWhen = "photosSelected",
      },
	},

    LrHelpMenuItems = {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompareHelp=DevSettingsCompare Help",
		file = "Help.lua",
	},

	VERSION = { display="20101118(SVN 178M)" },
    LrAlsoUseBuiltInTranslations = true,
}


	