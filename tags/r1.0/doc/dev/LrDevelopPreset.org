#-*- mode:org; -*-
#+PROPERTY: DataType boolean number string table Headline
#+PROPERTY: HeadlineType Section SubSection
#+PROPERTY: Label
#+TAGS: boolean number string table missing
#+PRIORITIES: 1 5 3
#+STARTUP: content

* Tools
** Crop Overlay (Crop and Straighten)
*** CropAngle															:number:
*** CropBottom															:number:
*** CropLeft															:number:
*** CropRight															:number:
*** CropTop																:number:
*** Orientation															:string:
	Test, unknown where orientation belongs to
** Spot Removal
*** RetouchInfo															 :table:
** Red Eye Correction
*** RedEyeInfo															 :table:
** Graduated Filter
** Adjustment Brush

* Basic
** Treatment (Color|Black & White)									   :missing:
** WhiteBalance															:string:
** Temperature															:number:
** Tint																	:number:
** Exposure																:number:
** HighlightRecovery													:number:
** FillLight															:number:
** Shadows																:number:
** Brightness															:number:
** Contrast																:number:
** Clarity																:number:
** Vibrance																:number:
** Saturation															:number:

* Tone Curve
** ToneCurveName														:string:
** ToneCurve															 :table:
	"ToneCurve.1",
	"ToneCurve.2",
	"ToneCurve.3",
	"ToneCurve.4",
	"ToneCurve.5",
	"ToneCurve.6",
	"ToneCurve.7",
	"ToneCurve.8",
	"ToneCurve.9",
	"ToneCurve.10",
	"ToneCurve.11",
	"ToneCurve.12",
	"ToneCurve.13",
	"ToneCurve.14",
	"ToneCurve.15",
	"ToneCurve.16",
	"ToneCurve.17",
	"ToneCurve.18",
	"ToneCurve.19",
	"ToneCurve.20",
	"ToneCurve.21",
	"ToneCurve.22",
	"ToneCurve.23",
	"ToneCurve.24",
	"ToneCurve.25",
	"ToneCurve.26",
	"ToneCurve.27",
	"ToneCurve.28",
	"ToneCurve.29",
	"ToneCurve.30",
	"ToneCurve.31",
	"ToneCurve.32",
** ParametricShadowSplit												:number:
** ParametricMidtoneSplit												:number:
** ParametricHighlightSplit												:number:
** ParametricHighlights													:number:
** ParametricLights														:number:
** ParametricDarks														:number:
** ParametricShadows													:number:

* HSL / Color / B & W
** HSL
*** Hue
**** HueAdjustmentRed													:number:
**** HueAdjustmentOrange												:number:
**** HueAdjustmentYellow												:number:
**** HueAdjustmentGreen													:number:
**** HueAdjustmentAqua													:number:
**** HueAdjustmentBlue													:number:
**** HueAdjustmentPurple												:number:
**** HueAdjustmentMagenta												:number:
*** Saturation
**** SaturationAdjustmentRed											:number:
**** SaturationAdjustmentOrange											:number:
**** SaturationAdjustmentYellow											:number:
**** SaturationAdjustmentGreen											:number:
**** SaturationAdjustmentAqua											:number:
**** SaturationAdjustmentBlue											:number:
**** SaturationAdjustmentPurple											:number:
**** SaturationAdjustmentMagenta										:number:
*** Luminance
**** LuminanceAdjustmentRed												:number:
**** LuminanceAdjustmentOrange											:number:
**** LuminanceAdjustmentYellow											:number:
**** LuminanceAdjustmentGreen											:number:
**** LuminanceAdjustmentAqua											:number:
**** LuminanceAdjustmentBlue											:number:
**** LuminanceAdjustmentPurple											:number:
**** LuminanceAdjustmentMagenta											:number:
*** All
	Superfluous
** Color
*** Red
**** HueAdjustmentRed													:number:
**** SaturationAdjustmentRed											:number:
**** LuminanceAdjustmentRed												:number:
*** Orange
**** HueAdjustmentOrange												:number:
**** SaturationAdjustmentOrange											:number:
**** LuminanceAdjustmentOrange											:number:
*** Yellow
**** HueAdjustmentYellow												:number:
**** SaturationAdjustmentYellow											:number:
**** LuminanceAdjustmentYellow											:number:
*** Green
**** HueAdjustmentGreen													:number:
**** SaturationAdjustmentGreen											:number:
**** LuminanceAdjustmentGreen											:number:
*** Aqua
**** HueAdjustmentAqua													:number:
**** SaturationAdjustmentAqua											:number:
**** LuminanceAdjustmentAqua											:number:
*** Blue
**** HueAdjustmentBlue													:number:
**** SaturationAdjustmentBlue											:number:
**** LuminanceAdjustmentBlue											:number:
*** Purple
**** HueAdjustmentPurple												:number:
**** SaturationAdjustmentPurple											:number:
**** LuminanceAdjustmentPurple											:number:
*** Magenta
**** HueAdjustmentMagenta												:number:
**** SaturationAdjustmentMagenta										:number:
**** LuminanceAdjustmentMagenta											:number:
*** All
	Superfluous
** B & W
   :PROPERTIES:
   :Label:    Black & White Mix
   :END:
**** GrayMixerRed														:number:
**** GrayMixerOrange													:number:
**** GrayMixerYellow													:number:
**** GrayMixerGreen														:number:
**** GrayMixerAqua														:number:
**** GrayMixerBlue														:number:
**** GrayMixerPurple													:number:
**** GrayMixerMagenta													:number:

* Split Toning
** Highlights
*** SplitToningHighlightHue												:number:
*** SplitToningHighlightSaturation										:number:
*** SplitToningBalance													:number:
** Shadows
*** SplitToningShadowHue												:number:
*** SplitToningShadowSaturation											:number:

* Details
** Sharpening
*** Sharpness															:number:
*** SharpenRadius														:number:
*** SharpenDetail														:number:
*** SharpenEdgeMasking													:number:
** Noise Reduction
*** LuminanceSmoothing													:number:
*** LuminanceNoiseReductionDetail										:number:
*** LuminanceNoiseReductionContrast										:number:
*** ColorNoiseReduction													:number:
*** ColorNoiseReductionDetail											:number:

* Lens Corrections
** Profile | Manual
*** Profile
***** Setup (Default|Auto|Custom)									   :missing:
***** Lens Profile
****** Make														:missing:string:
	   Typo?
****** Model													:missing:string:
****** Profile												   :missing::string:
**** Amount 
***** Distortion												:missing:number:
***** C. Aberration												:missing:number:
***** Vignetting												:missing:number:

*** Manual
**** Transform
***** Distortion												:missing:number:
***** Vertical													:missing:number:
***** Horizontal												:missing:number:
***** Rotate													:missing:number:
***** Scale														:missing:number:
***** Constrain Crop										   :missing:boolean:
**** Lens Vignetting
***** VignetteAmount													:number:
***** VignetteMidpoint													:number:
**** Chromatic Aberration
***** ChromaticAberrationR												:number:
***** ChromaticAberrationB												:number:
***** Defringe															:number:
	  (Off|Highlight Edges|All Edges)

* Effects
** Post-Crop Vignetting
*** PostCropVignetteStyle												:number:
	(Highlight Priority|Color Priority|Paint Overlay)
*** PostCropVignetteAmount												:number:
*** PostCropVignetteMidpoint											:number:
*** PostCropVignetteRoundness											:number:
*** PostCropVignetteFeather												:number:
*** PostCropVignetteHighlightContrast									:number:
** Grain
*** GrainAmount															:number:
*** GrainSize															:number:
*** GrainFrequency														:number:
	Roughness
* Camera Calibration
** ProcessVersion														:string:
   (2003|2010)
** CameraProfile														:string:
** Shadows
*** ShadowTint															:number:
** Red Primary
*** RedHue																:number:
*** RedSaturation														:number:
** Green Primary
*** GreenHue															:number:
*** GreenSaturation														:number:
** Blue Primary
*** BlueHue																:number:
*** BlueSaturation														:number:

* Unused 'develop' settings
** AutoBrightness													   :boolean:
** AutoContrast														   :boolean:
** AutoExposure														   :boolean:
** AutoShadows														   :boolean:
** ConvertToGrayscale												   :boolean:
** EnableCalibration												   :boolean:
** EnableLensCorrections 											   :boolean:
** EnableColorAdjustments											   :boolean:
** EnableDetail														   :boolean:
** EnableEffects													   :boolean:
** EnableGradientBasedCorrections									   :boolean:
** EnableGrayscaleMix												   :boolean:
** EnablePaintBasedCorrections										   :boolean:
** EnableRedEye														   :boolean:
** EnableRetouch													   :boolean:
** EnableSplitToning												   :boolean:

* Unclassified 'preset' settings
** AutoGrayscaleWeights												   :boolean:
** AutoTonality														   :boolean:
** Tone Curve
*** ToneShadowSplit														:number:
*** ToneMidtoneSplit													:number:
*** ToneHighlightSplit													:number:
*** ToneHighlights														:number:
*** ToneLights															:number:
*** ToneDarks															:number:
*** ToneShadows															:number:
*** PointCurve (Linear|Medium Contrast|Strong Contrast)				   :missing:
