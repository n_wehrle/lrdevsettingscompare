Why should I use LrDevSettingsCompare
=====================================

We developed this plugin to get to know how presets work: why does this particular preset such a pretty job, what is the magic behind the other preset? Our plugin compares the BEFORE with the AFTER and just shows the differences. By analyzing these differences we hope to learn how to create better presets.

![coverpic](https://bitbucket.org/n_wehrle/lrdevsettingscompare/raw/master/coverpic.png)

Project information
===================

Programming: Norman Wehrle (Lead), Matthias Saft  
Logo Design: Mathias Griessen

Usage overview
==============

There are just a few simple steps:

1.  Install the plugin
2.  Optional: configure the plugin (there are reasonable defaults
    already)
3.  Optional: configure a reference preset
4.  Choose exactly one or two photos
5.  Click the menu entry in the dropdown menu

For further information please read the embedded help in the plugin's configuration dialog.
The old doc is still available on sourceforge: http://lidiplu.sourceforge.net/