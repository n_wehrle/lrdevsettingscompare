-- -*- mode: lua; coding: utf-8; indent-tabs-mode: nil; -*-
--
-- Translation for de
--
--
--

-- LR5 Settings
"$$$/lidiplu/Setting/Exposure2012=Belichtung"
"$$$/lidiplu/Setting/Contrast2012=Kontrast"
"$$$/lidiplu/Setting/Highlights2012=Lichter"
"$$$/lidiplu/Setting/Shadows2012=Tiefen"
"$$$/lidiplu/Setting/Whites2012=Weiss"
"$$$/lidiplu/Setting/Blacks2012=Schwarz"
"$$$/lidiplu/Setting/Clarity2012=Klarheit"
"$$$/AgDevelop/Panel/CalibrationDescription=PV2012(6.7), PV2010(5.7), PV2003(5.0)"

-- Plugin
"$$$/lidiplu/PluginName=DevSettingsCompare"
"$$$/lidiplu/LocalHelp=help/index_de.html"

-- Menu Entries
"$$$/lidiplu/Menu/DevSettingsCompare=Entwicklungseinstellungen vergleichen"
"$$$/lidiplu/Menu/DevSettingsCompareHelp=DevSettingsCompare Hilfe"

-- Windows
"$$$/lidiplu/Window/Main/Title=Vergleich der Entwicklungseinstellungen"
"$$$/lidiplu/Window/Preset/Title=Preset auswählen"

-- Messages
"$$$/lidiplu/Warning/RefSelection=Bitte schliessen Sie diesen Dialog, wählen Sie vorab ein Foto aus, von dem^ndie Ettwicklungseinstellungen kopiert werden sollen^nund öffnen Sie diesen Dialog erneut."
"$$$/lidiplu/Warning/Selection=Bitte wählen Sie genau ein oder zwei Fotos aus."
"$$$/lidiplu/Warning/RefSettingsPresetNotFound=Keine Vorgabe ^{^1^} gefunden.^nBitte erstellen Sie eine Vorgabe mit Ihren Standard-Entwicklungseinstellungen, und speichern diese unter dem Namen ^{^1^}."
"$$$/lidiplu/Warning/RefSettingsNotFound=Referenz-Einstellungen nicht gefunden. Bitte legen Sie die Referenz-Einstellungen im Plug-in Manager fest oder selektieren Sie ein zweites Foto."
"$$$/lidiplu/Message/NoDelta=Keine Unterschiede gefunden."
"$$$/lidiplu/Warning/NoPreset=^{^1^} ist kein gültiges Preset"

-- TableHead
"$$$/lidiplu/Selection/Selection=Auswahl beim Start"
"$$$/lidiplu/Selection/Active=Aktives Foto"
"$$$/lidiplu/Selection/Other=Anderes Foto"

-- Config
"$$$/lidiplu/Config/Headline=Konfiguration"
"$$$/lidiplu/Config/Option/DebugMode=Debug Mode aktivieren"
"$$$/lidiplu/Config/Option/Threshold=Unterschiede um auf GUI mit Tabs zu wechseln"
"$$$/lidiplu/Config/Option/ShowSummary=Änderungen in Gradationskurve und selektive Anpassungen zusammenfassen"
"$$$/lidiplu/Config/Option/GUISettingsName=GUI Einstellungen"
"$$$/lidiplu/Config/Option/RefSettingsName=Referenz Einstellungen"
"$$$/lidiplu/Config/Action/Bugreport=Bug melden"
"$$$/lidiplu/Config/Action/Help=Hilfe"
"$$$/lidiplu/Config/Action/RefSettingsPreset=Aus Vorgabe"
"$$$/lidiplu/Config/Action/RefSettingsPhoto=Von Foto"
"$$$/lidiplu/Config/Action/RefSettingsClear=Löschen"
"$$$/lidiplu/Config/Label/CurrentRefFrom=Referenz aus ^1 ^{^2^} ^3"
"$$$/lidiplu/Config/Label/RefFromPhoto/ConfigView=Foto"
"$$$/lidiplu/Config/Label/RefFromPhoto/CompareView=Referenz"
"$$$/lidiplu/Config/Label/RefFromPreset/ConfigView=Preset"
"$$$/lidiplu/Config/Label/RefFromPreset/CompareView=Preset"
"$$$/lidiplu/Config/Label/CurrentRefNil=Keine Referenz gespeichert"
"$$$/lidiplu/Config/Progress/SavingRef=Speichere Referenz"

-- Curves and retouches
"$$$/lidiplu/Summary/ToneCurve/Changed=Punkte verschoben"
"$$$/lidiplu/Summary/ToneCurve/Unchanged=Unverändert"
"$$$/AgDevelop/Panel/ToneCurvePV2012=Gradationskurve"

-- Link mismatching ZStrings
"$$$/lidiplu/Setting/ParametricHighlights=$$$/AgCameraRawUI/ToneHighlights"
"$$$/lidiplu/Setting/ParametricLights=$$$/AgCameraRawUI/ToneLights"
"$$$/lidiplu/Setting/ParametricDarks=$$$/AgCameraRawUI/ToneDarks"
"$$$/lidiplu/Setting/ParametricShadows=$$$/AgCameraRawUI/ToneShadows"
"$$$/lidiplu/Setting/ToneCurveName=$$$/AgCameraRawUI/PointCurve"
"$$$/lidiplu/Setting/ToneCurveName2012=$$$/AgCameraRawUI/PointCurve"
"$$$/lidiplu/Setting/LensProfileName=$$$/AgDevelop/CameraRawPanel/LensCorrection/Profile"
"$$$/lidiplu/Setting/GrainFrequency=$$$/AgCameraRawUI/GrainRoughness"

-- No default zString
"$$$/lidiplu/Setting/CropTop=Oben"
"$$$/lidiplu/Setting/CropRight=Rechts"
"$$$/lidiplu/Setting/CropBottom=Unten"
"$$$/lidiplu/Setting/CropLeft=Links"
"$$$/lidiplu/Setting/ParametricShadowSplit=Obergrenze Tiefen"
"$$$/lidiplu/Setting/ParametricMidtoneSplit=Obergrenze Dunkle Mitteltöne"
"$$$/lidiplu/Setting/ParametricHighlightSplit=Untergrenze Lichter"

