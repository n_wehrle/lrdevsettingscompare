--[[
  Compares the development settings of
  two different photos.

  Plugin:  LrDevSettingsCompare
  Version: 20140426(GIT 45e9c36)
  Authors: Norman Wehrle, Matthias Saft

  Done:
    - compares two photos
    - compares a photo with a default preset
    - shows differences side by side
    - does not show similarities

  ======================================================
  
  Todo:
    - calc delta and render:
        Clarity  |  1  |  17(+16)
    - beautify GUI more
        color coding (small change, big change, value only here)
        sliders or charts instead of bare numbers
    - compare dev presets (2nd menu entry = 2nd script)
    - statistics?
    - apply presets in some way?
]]
return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 3.0,

	-- todo: identifier to be discussed
    LrToolkitIdentifier = 'ch.dslr.lightroom.lrdevsettingscompare',
    LrPluginName = LOC "$$$/lidiplu/PluginName=DevSettingsCompare",
    LrPluginInfoUrl = "https://bitbucket.org/n_wehrle/lrdevsettingscompare",
	
    LrPluginInfoProvider = "PluginInfoProvider.lua",

	-- only in file menu until yet
    LrExportMenuItems = {
      {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompare=Compare Development Settings",
		file = "PluginMain.lua",
        enabledWhen = "photosSelected",
      },
--      {
--		title = "Debug DevSettings",
--		file = "DebugDevSettings.lua",
--        enabledWhen = "photosSelected",
--      },
	},

    LrHelpMenuItems = {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompareHelp=DevSettingsCompare Help",
		file = "Help.lua",
	},

	VERSION = { display="20140426(GIT 45e9c36)" },
    LrAlsoUseBuiltInTranslations = true,
}


	