-- global
local LrLogger = import 'LrLogger'

-- module
local LogUtil = {}

-- default logger
local Log = LrLogger('LrDevSettingsCompareLogger')
Log:enable("logfile")

-- instead of different functions for trace, debug, warn, error
function LogUtil.getDefaultLogger()
  return Log
end


-- stolen from 'lua gems'
function LogUtil.vardump (value, depth, key)
  local linePrefix = ""
  local spaces = ""

  if key ~= nil then
    linePrefix = "["..key.."] = "
  end

  if depth == nil then
    depth = 0
  else
    depth = depth + 1
    for i=1, depth do spaces = spaces .. "  " end
  end

  if type(value) == 'table' then
    mTable = getmetatable(value)
    if mTable == nil then
      Log:trace(spaces ..linePrefix.."(table) ")
    else
      Log:trace(spaces .."(metatable) ")
        value = mTable
    end
    for tableKey, tableValue in pairs(value) do
      LogUtil.vardump(tableValue, depth, tableKey)
    end
  elseif type(value)	== 'function' or
      type(value)	== 'thread' or
      type(value)	== 'userdata' or
      value		== nil
  then
    Log:trace(spaces..tostring(value))
  else
    Log:trace(spaces..linePrefix.."("..type(value)..") "..tostring(value))
  end
end

return LogUtil

