-- global
local LrView = import "LrView"
local LrHttp = import "LrHttp"
local LrBinding = import "LrBinding"
local LrPrefs = import "LrPrefs"
local LrDialogs = import 'LrDialogs'
local LrTasks = import 'LrTasks'
local LrFunctionContext = import 'LrFunctionContext'
local LrApplication = import 'LrApplication'
local LrPathUtils = import 'LrPathUtils'

-- local
local PluginProperties = require "PluginProperties"
local FrontendUtil = require 'FrontendUtil'
local PrefsUtil = require 'PrefsUtil'
local RefSettingsUtil = require 'RefSettingsUtil'
local PresetChooseDialog = require 'PresetChooseDialog'

-- todo: setFromPreset and setFromFoto can be refactored
-- to use the same codebase
local function setFromPreset()
  local presetName = PresetChooseDialog.choose()
  if (nil ~= presetName) then

    LrFunctionContext.postAsyncTaskWithContext("LrDevSettingsCompareAsync", function(context)
      -- errormsgs do not conflict progressbar
      LrDialogs.attachErrorDialogToFunctionContext(context)

    -- modal progress window
      local progressScope = LrDialogs.showModalProgressDialog({
        title = LOC"$$$/lidiplu/Config/Progress/SavingRef=Saving Reference",
        cannotCancel = true,
        functionContext = context
      })
      progressScope:setIndeterminate()

      -- workaround to always show the progress bar
      LrTasks.sleep(0.1)

      -- do the long running work
      -- should never happen
      local refType, refInfo, refAddInfo = RefSettingsUtil.setRefSettingsFromPreset(presetName)
      assert (refType and refInfo, "Reference info must not be null" )

      local props = LrPrefs.prefsForPlugin()

      -- saved to be used in tablehead in comparison view
      props.refSettingsMeta = {refType, refInfo, refAddInfo}
    end)
  end
end


-- todo: setFromPreset and setFromFoto can be refactored
-- to use the same codebase
local function setFromPhoto()
  local catalog = LrApplication.activeCatalog()
  local targetPhoto = catalog:getTargetPhoto()

  if (targetPhoto) then
    LrFunctionContext.postAsyncTaskWithContext("LrDevSettingsCompareAsync", function(context)
      -- errormsgs do not conflict progressbar
      LrDialogs.attachErrorDialogToFunctionContext(context)

    -- modal progress window
      local progressScope = LrDialogs.showModalProgressDialog({
        title = LOC"$$$/lidiplu/Config/Progress/SavingRef=Saving Reference",
        cannotCancel = true,
        functionContext = context
      })
      progressScope:setIndeterminate()

      -- workaround to always show the progress bar
      LrTasks.sleep(0.1)

      -- do the long running work
      local refType, refInfo, refAddInfo = RefSettingsUtil.setRefSettingsFromSelectedPhoto(targetPhoto)
      assert (refType and refInfo, "Reference info must not be null" )

      local props = LrPrefs.prefsForPlugin()

      -- saved to be used in tablehead in comparison view
      props.refSettingsMeta = {refType, refInfo, refAddInfo}
    end)
  else
    local message = LOC("$$$/lidiplu/Warning/RefSelection=" ..
            "Please select a photo to copy the development settings from.")
    LrDialogs.message(message, nil, "warning")
  end
end


local function disableRef()
  local props = LrPrefs.prefsForPlugin()
  props.refSettingsMeta = nil
end


local function sectionsForTopOfDialog(f, _)
  local props = LrPrefs.prefsForPlugin()

  -- if more differences found than configured, show in tabs instead of big table
  props.threshold = PrefsUtil.getThreshold()
  props.debugMode = PrefsUtil.isDebugModeEnabled()
  props.showSummary = PrefsUtil.isShowSummaryEnabled()

  return {
      {
      title = LOC "$$$/lidiplu/Config/Headline=Configuration",
      bind_to_object = props,
      place = "horizontal",
      spacing = 20,

        f:column {
          spacing = f:control_spacing(),
          fill_horizontal = 1,
          f:group_box{
            title = LOC "$$$/lidiplu/Config/Option/GUISettingsName=GUI Settings",
            show_title = true,
            fill_horizontal = 1,
            f:row {
              f:checkbox{
                title = LOC "$$$/lidiplu/Config/Option/ShowSummary=Summarize tonecurve changes and retouchings",
                value = LrView.bind("showSummary"),
              },
            },
            f:row {
              f:static_text{
                title = LOC "$$$/lidiplu/Config/Option/Threshold=Differences needed to switch to tabbed GUI",
              },
              f:slider{
                value = LrView.bind("threshold"),
                min = 1,
                max = 50,
                integral = true,
                fill_horizontal = 1,
              },
              f:edit_field{
                value = LrView.bind("threshold"),
                min = 1,
                max = 50,
                width_in_digits = 2,
                increment = 1,
                large_increment = 5,
                precision = 0,
              },
            },
          },
          f:group_box{
            title = LOC "$$$/lidiplu/Config/Option/RefSettingsName=Reference settings",
            show_title = true,
            fill_horizontal = 1,
            f:row {
              f:static_text{
                title = LrView.bind {
                  key = "refSettingsMeta",
                  transform = function(value, fromTable)
                    if fromTable then
                      if nil ~= value then
                        assert ("table" == type(value), "prefs meta info has to be a table")
                        local refType, refInfo, refAddInfo = unpack(value)
                        return LOC("$$$/lidiplu/Config/Label/CurrentRefFrom=" ..
                                "Reference from ^1 ^{^2^} ^3",
                                PluginProperties.RefType[refType].configViewLabel,
                                refInfo,
                                refAddInfo and (" (" .. refAddInfo .. ")") or ""
                        )
                      end
                      return LOC("$$$/lidiplu/Config/Label/CurrentRefNil=No Reference saved yet")
                    end
                    return LrBinding.kUnsupportedDirection
                  end,
                },
                width_in_chars = 40,
              },
            },
            f:row {
              spacing = f:control_spacing(),
              f:push_button{
                width = 110,
                title = LOC "$$$/lidiplu/Config/Action/RefSettingsPreset=From preset",
                enabled = true,
                action = setFromPreset
              },
              f:push_button{
                width = 110,
                title = LOC "$$$/lidiplu/Config/Action/RefSettingsPhoto=From photo",
                enabled = true,
                action = setFromPhoto
              },
              f:push_button{
                width = 110,
                title = LOC "$$$/lidiplu/Config/Action/RefSettingsClear=Clear",
                enabled = true,
                action = disableRef
              },
            },
          },
        },
        f:column {
          spacing = f:control_spacing(),
          f:row {
            margin = 0,
            spacing = 0,
            f:spacer{
              width = WIN_ENV and 20 or 5,
            },
            f:picture{
              value = LrPathUtils.child(_PLUGIN.path,"img/round_80.png"),
              tooltip = PluginProperties.Copyright,
            },
            f:spacer{
              width = WIN_ENV and 20 or 1,
            },
          },
          f:push_button{
            width = 120,
            title = LOC "$$$/lidiplu/Config/Action/Help=Help",
            enabled = true,
            action = function()
              FrontendUtil.openInDefaultApp(PluginProperties.UrlForLocalHelp)
            end,
          },
          f:push_button{
            width = 120,
            title = LOC "$$$/lidiplu/Config/Action/Bugreport=Report Bug",
            enabled = true,
            action = function()
              LrHttp.openUrlInBrowser(PluginProperties.UrlForBugReport)
            end,
          },
          f:checkbox{
            title = LOC "$$$/lidiplu/Config/Option/DebugMode=Enable Debug Mode",
            value = LrView.bind("debugMode"),
          },
        },
    },
  }
end


return {
  sectionsForTopOfDialog = sectionsForTopOfDialog,
}