local LrApplication = import "LrApplication"

local PresetUtil = {}

function PresetUtil.presetForName(presetName)
  assert (nil ~= presetName, "Presetname must not be null")
  for _, presetFolder in ipairs(LrApplication.developPresetFolders()) do
    local presets = presetFolder:getDevelopPresets()
    for _, preset in ipairs(presets) do
       if (presetName == preset:getName()) then
         return preset
       end
    end
  end
  return nil
end

function PresetUtil.getAllPresets()
  local allNames = {}
  local allUuids = {}
  for _, presetFolder in ipairs(LrApplication.developPresetFolders()) do
    local presets = presetFolder:getDevelopPresets()
    for _, preset in ipairs(presets) do
      table.insert(allNames, preset:getName())
      table[preset:getName()] = preset:getUuid()
    end
  end
  return allNames, allUuids
end

return PresetUtil