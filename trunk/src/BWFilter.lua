local FilterUtil = require 'FilterUtil'
local BWFilter = {}

local filterName = "BWFilter"
local filterEnabled = true
local triggerTable = nil
local filterTable = nil

-----------------------------------------------------------------------
local function mustApplyFilter(_delta)
  if (filterEnabled) then
    -- lazy
    if (nil == triggerTable) then
      filterTable, triggerTable = FilterUtil.buildTables(filterName)
    end
    for _,trigger in ipairs(triggerTable) do
      if (nil ~= _delta[trigger]) then
        return true
      end
    end
  end
  return false
end


-----------------------------------------------------------------------
function BWFilter.filter(_delta)
  if mustApplyFilter(_delta) then
    -- lazy
    if (nil == filterTable) then
      filterTable, triggerTable = FilterUtil.buildTables(filterName)
    end

    for _,filter in ipairs(filterTable) do
      if (nil ~= _delta[filter]) then
        assert ("table" == type(_delta[filter]), "table expected")
        local val1, val2 = unpack (_delta[filter])
        if (0 == val1 and nil == val2) then
          _delta[filter] = nil
        elseif (nil == val1 and 0 == val2) then
          _delta[filter] = nil
        end
      end
    end
  end
  return _delta
end

-----------------------------------------------------------------------
return BWFilter