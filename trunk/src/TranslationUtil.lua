--global
local LrLocalization = import 'LrLocalization'

--local
local PluginProperties = require 'PluginProperties'
local PrefsUtil = require 'PrefsUtil'

local TranslationUtil = {
  undefined = "__undef__",
}


local function splitOnFirstPeriod(input)
  local index = string.find(input, "%.")
  if (index and index > 1) then
    return (string.sub(input, 1, index - 1)),
           (string.sub(input, index))
  else
    return input
  end
end


local function lastWord(input)
  local result
  for part in string.gmatch(input, "[%a&]+") do
    result = part
  end
  return result
end


local function platformSpecificString(input)
  return MAC_ENV and string.gsub(input, "&&", "&") or input
end


function TranslationUtil.translateGeneric(zString)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or lastWord(zString)
  local localizedString = LOC(string.format("%s=%s", zString, defaultVal))
  return platformSpecificString(localizedString)
end


function TranslationUtil.translateMeta(meta)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or meta
  local localizedString =  LOC(string.format("$$$/AgLibrary/MetadataFormatters/%s=%s", meta, defaultVal))
  return platformSpecificString(localizedString)
end


function TranslationUtil.translateSetting(setting, blocktitle)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or setting

  local patterns = {
    "$$$/AgCameraRawUI/%s=%s",
    "$$$/AgCameraRawNamedSettings/CameraRawSettingMapping/%s=%s",
    "$$$/AgCameraRawNamedSettings/CameraRawSettingMapping/%sCalibration=%s",
    "$$$/AgDevelop/Panel/%s=%s",
    "$$$/lidiplu/Setting/%s=%s",
  }

  local localizedString = TranslationUtil.undefined
  for _,pattern in ipairs(patterns) do
    localizedString = LOC(string.format(pattern, setting, TranslationUtil.undefined))
    if (TranslationUtil.undefined ~= localizedString) then
      break
    end
  end

  -- depending on preferences return key as fallback or __undef__
  localizedString = (TranslationUtil.undefined == localizedString and defaultVal) or localizedString

  if (3 <= string.len(localizedString) and "$$$" == string.sub(localizedString, 1, 3)) then
    localizedString = LOC(string.format("%s=%s", localizedString, defaultVal))
  end
  
  return platformSpecificString(localizedString)
end


-- to handle ToneCurve.27
function TranslationUtil.translateEnumeratedSetting(setting, blocktitle)
  local counter
  setting, counter = splitOnFirstPeriod(setting)
  return counter and (TranslationUtil.translateSetting(setting, blocktitle) .. counter)
    or TranslationUtil.translateSetting(setting, blocktitle)
end

return TranslationUtil