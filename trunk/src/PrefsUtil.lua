local LrPrefs = import 'LrPrefs'

-- defaults on first load
local debugModeOnFirstStart = false
local showSummaryOnFirstStart = false
local thresholdOnFirstStart = 20

local PrefsUtil = {}

function PrefsUtil.isDebugModeEnabled()
  if (nil == LrPrefs.prefsForPlugin().debugMode) then
    return debugModeOnFirstStart
  end
  return LrPrefs.prefsForPlugin().debugMode
end

function PrefsUtil.getRefSettingsMeta()
  return LrPrefs.prefsForPlugin().refSettingsMeta
end

function PrefsUtil.isShowSummaryEnabled()
  if (nil == LrPrefs.prefsForPlugin().showSummary) then
    return showSummaryOnFirstStart
  end
  return LrPrefs.prefsForPlugin().showSummary
end

function PrefsUtil.getThreshold()
  return LrPrefs.prefsForPlugin().threshold or thresholdOnFirstStart
end

return PrefsUtil