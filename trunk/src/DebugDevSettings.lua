local LrApplication = import 'LrApplication'
local LrTasks = import 'LrTasks'

local LogUtil = require 'LogUtil'
local BackendUtil = require 'BackendUtil'

-- main
local function main(context)
    local catalog = LrApplication.activeCatalog()
    local selectedPhotos = catalog:getTargetPhotos()
    if (#selectedPhotos == 2) then
        local photoActive = selectedPhotos[1]
        local photoOther = selectedPhotos[2]

        -- active photo is always the first one
        if (catalog:getTargetPhoto() == photoOther) then
            photoActive, photoOther = photoOther, photoActive
        end

        local devSettingsActive = photoActive:getDevelopSettings()
        local devSettingsOther = photoOther:getDevelopSettings()

        assert (nil ~= devSettingsActive, "Devsettings for active photo must not be null")
        assert (nil ~= devSettingsOther,  "Devsettings for other photo must not be null")

        -- iterate devsettings of first photo, compare with second, save delta in table
        local deltaBack = BackendUtil.flattenedDelta(devSettingsActive, devSettingsOther)

        -- iterate devsettings of second photo, compare with first, use the same table to save
        local deltaBackForth = BackendUtil.flattenedDelta(devSettingsOther, devSettingsActive, deltaBack, nil, true)
        LogUtil.vardump(deltaBackForth)
        LogUtil.getDefaultLogger():trace("ProcessVersion" .. photoActive:getDevelopSettings().ProcessVersion)
    end
end

-- async (required by some functions in the method body)
-- withContext (needed for error handling)
-- LrFunctionContext.postAsyncTaskWithContext("LrDevSettingsCompareAsync", main)

-- async (required by some functions in the method body)
-- no context, but default errorhandling by dialogs activated
LrTasks.startAsyncTask(main)