local FilterUtil = require 'FilterUtil'
local PresetFilter = {}

local filterName = "PresetFilter"
local filterEnabled = true
local filterTable = nil

-----------------------------------------------------------------------
local function mustApplyFilter()
  -- todo photoOther won't be global for all times
  return filterEnabled and (nil == photoOther)
end


-----------------------------------------------------------------------
function PresetFilter.filter(_delta)
  if mustApplyFilter() then
    -- lazy
    if (nil == filterTable) then
      filterTable = FilterUtil.buildTables(filterName)
    end

    for _,filter in ipairs(filterTable) do
      _delta[filter] = nil
    end
  end
  return _delta
end

-----------------------------------------------------------------------
return PresetFilter