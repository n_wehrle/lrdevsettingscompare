local RefSettingsUtil = require 'RefSettingsUtil'

return {
    NoProcess2012Filter = function()
        if (nil == photoOther) then
            local refSettingsPreset = RefSettingsUtil.getRefSettingsPreset()
            local devSettingsPreset = refSettingsPreset:getSetting()
            return ("6.7" ~= photoActive:getDevelopSettings().ProcessVersion)
                    and ("6.7" ~= devSettingsPreset.ProcessVersion)
        else
            return ("6.7" ~= photoActive:getDevelopSettings().ProcessVersion)
                    and ("6.7" ~= photoOther:getDevelopSettings().ProcessVersion)
        end
    end,
    OnlyProcess2012Filter = function()
        if (nil == photoOther) then
            local refSettingsPreset = RefSettingsUtil.getRefSettingsPreset()
            local devSettingsPreset = refSettingsPreset:getSetting()
            return ("6.7" == photoActive:getDevelopSettings().ProcessVersion)
                    and ("6.7" == devSettingsPreset.ProcessVersion)
        else
            return ("6.7" == photoActive:getDevelopSettings().ProcessVersion)
                    and ("6.7" == photoOther:getDevelopSettings().ProcessVersion)
        end
    end
}