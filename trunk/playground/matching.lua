local testString = "$$$/AgDevelop/Crop/Crop&Straighten"
local result

for part in string.gmatch(testString, "[%a&]+") do
  result = part
end

print (result)

local function splitOnFirstPeriod(input)
  local index = string.find(input, "%.")
  if (index and index > 1) then
    return (string.sub(input, 1, index - 1)),
           (string.sub(input, index))
  else
    return input
  end
end

print (splitOnFirstPeriod("ToneCurve.2"))
print (splitOnFirstPeriod("OhnePunkt"))
