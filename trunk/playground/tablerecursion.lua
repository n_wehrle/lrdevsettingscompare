vardump = function (value, depth, key)
  local linePrefix = ""
  local spaces = ""

  if key ~= nil then
    linePrefix = "["..key.."] = "
  end

  if depth == nil then
    depth = 0
  else
    depth = depth + 1
    for i=1, depth do spaces = spaces .. "  " end
  end

  if type(value) == 'table' then
    mTable = getmetatable(value)
    if mTable == nil then
      print(spaces ..linePrefix.."(table) ")
    else
      print(spaces .."(metatable) ")
        value = mTable
    end
    for tableKey, tableValue in pairs(value) do
      vardump(tableValue, depth, tableKey)
    end
  elseif type(value)	== 'function' or
      type(value)	== 'thread' or
      type(value)	== 'userdata' or
      value		== nil
  then
    print(spaces..tostring(value))
  else
    print(spaces..linePrefix.."("..type(value)..") "..tostring(value))
  end
end

local function flattenedDelta(_tab1, _tab2, _delta, _prefix, _saveReversed)
  local _delta = _delta or {}
  local _prefix = _prefix or ""
  local _saveReversed = _saveReversed or false

  for k,v in pairs(_tab1) do
    local v2 = _tab2[k]
    if (v ~= v2) then
      if ("table" == type(v) and "table" == type(v2)) then
        flattenedDelta(v, v2, _delta, _prefix .. k .. ".", _saveReversed)


      elseif ("table" == type(v)) then
        if (nil ~= v2) then
          _delta[_prefix .. k] = _saveReversed and {v2} or {nil, v2}
        end
        flattenedDelta(v, {}, _delta, _prefix .. k .. ".", _saveReversed)
      elseif ("table" == type(v2)) then
        if (nil ~= v) then
          _delta[_prefix .. k] = _saveReversed and {nil, v} or {v}
        end
        flattenedDelta(v2, {}, _delta, _prefix .. k .. ".", not _saveReversed)


      else
        _delta[_prefix .. k] = _saveReversed and {v2, v} or {v, v2}
      end
    end
  end

  return _delta
end


local setting1 = {
  k1 = "v1",
  k2 = "v2",
  t1 = {
    t1k1 = "t1v1",
    t1k2 = "t1v2",
    t11 = {
      t11k1 = "t11v1",
      t11k2 = "t11v2",
      t11k3 = "t11v3",
      t11k5 = "t11v5",
    },
    t12 = {
      t12k1 = "t12v1",
    }
  },
  t2 = {
    t2k1 = "t2v1",
  },
  t3 = {},
  t4 = {
    t41 = {
      t41k1 = "t41v1",
      t41k2 = "t41v2",
    },
    t4k1 = "t4v1",
  },
  k3 = "v3",
  kv4 = "kv4",
  kv5 = {
    kv5t1 = {
      t1v1 = "v1"
    }
  },
}

local setting2 = {
  k1 = "v1",
  k2 = "v2",
  t1 = {
    t1k1 = "t1v1",
    t1k2 = "t1v2",
    t11 = {
      t11k1 = "t11v1",
      t11k2 = "t11v2x",
      t11k3 = "t11v3x",
      t11k4 = "t11v4",
    },
    t12 = {
      t12k1 = "t12v1",
    }
  },
  t2 = {
    t2k1 = "t2v1",
  },
  t3 = {},
  t4 = {
    t41 = {
      t41k1 = "t41v1",
      t41k2 = "t41v2",
    },
    t4k1 = "t4v1x",
  },
  k3 = "v3x",
  kv4 = {
    kv4t1 = {
      t1v1 = "v1" 
    }
  },
  kv5 = "kv5",
}

local setting3 = {
  kv4 = {
    kv4t1 = {
      t1v1 = "v1"
    }
  },
}


--[[ todo expected
t1.t11.t11k2 = {t11v2, t11v2x}
t1.t11.t11k3 = {t11v3, t11v3x}
t1.t11.t11k4 = {nil, t11v4}
t1.t11.t11k5 = {t11v5, nil}
k3 = {v3, v3x}
t4.t4k1 = {t4v1, t4v1x}
kv5 = {nil, kv5}
kv5.kv5k1 = {kv5v1, nil}
kv4 = {kv, nil}
kv4.kv4k1 = {nil, kv4v1}
--]]

local deltaBack = flattenedDelta(setting1, setting2)
local deltaBackForth = flattenedDelta(setting2, setting1, deltaBack, nil, true)
vardump(deltaBackForth)
-- local deltaBack = flattenedDelta(setting3, {})
-- vardump(deltaBack)