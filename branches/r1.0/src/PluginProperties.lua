local LrPathUtils = import "LrPathUtils"
local LrColor = import "LrColor"

return {
  UrlForBugReport = "http://sourceforge.net/projects/lidiplu/support",
  UrlForLocalHelp = LrPathUtils.child(_PLUGIN.path, LOC "$$$/lidiplu/LocalHelp=help/index.html"),
  Copyright = "(c) 2010 by N.Wehrle, M.Saft",

  -- not used yet
  UrlForHelp = "http://lidiplu.sourceforge.net",

  -- needed for descriptive string in pluginMan
  -- needed for table header in comparison view
  RefType = {
    {
      configViewLabel = LOC "$$$/lidiplu/Config/Label/RefFromPreset/ConfigView=preset",
      compareViewLabel = LOC "$$$/lidiplu/Config/Label/RefFromPreset/CompareView=preset",
    },
    {
      configViewLabel = LOC "$$$/lidiplu/Config/Label/RefFromPhoto/ConfigView=photo",
      compareViewLabel = LOC "$$$/lidiplu/Config/Label/RefFromPhoto/CompareView=reference",
    },
  },

  ColumnWeight = {0.4, 0.3, 0.3},
  TabWidthMac = 90,
  TabWidthWin = 70,
  HeadlineColor = LrColor(0.161, 0.271, 0.4),
  HeadlineFont = {name = "Helvetica", size=18},
  HeaderColor = LrColor(0),
  HeaderFont = "<system/small>",
}