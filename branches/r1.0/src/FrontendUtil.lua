--global
local LrApplication = import 'LrApplication'
local LrView = import 'LrView'
local LrShell = import 'LrShell'
local LrColor = import 'LrColor'

--local
local PluginProperties = require 'PluginProperties'
local TranslationUtil = require 'TranslationUtil'
local PrefsUtil = require 'PrefsUtil'

local FrontendUtil = {}
local col1sync, col2sync, col3sync = "col1sync", "col2sync", "col3sync"

local function subBlockMustBeRendered(_delta, _subBlock)
  for _,v in ipairs(_subBlock) do
    if (_delta[v] ~= nil) then
      return true
    end
  end
  return false
end

local function blockMustBeRendered(_delta, _block)
  for _,subBlock in ipairs(_block) do
    if (subBlockMustBeRendered(_delta, subBlock)) then
      return true
    end
  end
  return false
end

function FrontendUtil.showActivePhotoInNavigator(target)
  if (nil ~= photoActive and nil ~= photoOther) then
    local catalog = LrApplication.activeCatalog()
    catalog:setSelectedPhotos( photoActive, {photoOther})
  end
end

function FrontendUtil.showOtherPhotoInNavigator(target)
  if (nil ~= photoActive and nil ~= photoOther) then
    local catalog = LrApplication.activeCatalog()
    catalog:setSelectedPhotos( photoOther, {photoActive})
  end
end

local function appendTableHeadToElementList(f, _columnContent)
  local refType, refInfo, refAddInfo = "","",""
  local prefsMeta = PrefsUtil.getRefSettingsMeta()

  if (nil ~= prefsMeta) then
    assert ("table" == type(prefsMeta), "prefs meta info has to be a table")
    refType, refInfo, refAddInfo = unpack(prefsMeta)
  end

  table.insert(_columnContent, f:group_box {
    spacing = 0,
    fill_horizontal = 1,
    show_title = false,
    f:row {
      f:static_text {
        title = LOC "$$$/lidiplu/Selection/Selection=Selection",
        width = LrView.share(col1sync),
        fill_horizontal = PluginProperties.ColumnWeight[1],
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = LOC "$$$/lidiplu/Selection/Active=Active",
        width = LrView.share(col2sync),
        fill_horizontal = PluginProperties.ColumnWeight[2],
        alignment = "right",
        mouse_down = photoOther and FrontendUtil.showActivePhotoInNavigator or nil,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = photoOther and LOC "$$$/lidiplu/Selection/Other=Other"
                or PluginProperties.RefType[refType].compareViewLabel,
        width = LrView.share(col3sync),
        fill_horizontal = PluginProperties.ColumnWeight[3],
        alignment = "right",
        mouse_down = photoOther and FrontendUtil.showOtherPhotoInNavigator or nil,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
    },
    f:row {
      f:static_text {
        title = TranslationUtil.translateMeta("FileName"),
        width = LrView.share(col1sync),
        fill_horizontal = PluginProperties.ColumnWeight[1],
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = photoActive:getFormattedMetadata("fileName"),
        width = LrView.share(col2sync),
        fill_horizontal = PluginProperties.ColumnWeight[2],
        alignment = "right",
        mouse_down = photoOther and FrontendUtil.showActivePhotoInNavigator or nil,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = photoOther and photoOther:getFormattedMetadata("fileName")
                or refInfo,
        width = LrView.share(col3sync),
        fill_horizontal = PluginProperties.ColumnWeight[3],
        alignment = "right",
        mouse_down = photoOther and FrontendUtil.showOtherPhotoInNavigator or nil,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
    },
    f:row {
      f:static_text {
        title = TranslationUtil.translateMeta("CopyName"),
        width = LrView.share(col1sync),
        fill_horizontal = PluginProperties.ColumnWeight[1],
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = photoActive:getFormattedMetadata("copyName"),
        width = LrView.share(col2sync),
        fill_horizontal = PluginProperties.ColumnWeight[2],
        alignment = "right",
        mouse_down = FrontendUtil.showActivePhotoInNavigator,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
      f:static_text {
        title = photoOther and photoOther:getFormattedMetadata("copyName")
                or refAddInfo,
        width = LrView.share(col3sync),
        fill_horizontal = PluginProperties.ColumnWeight[3],
        alignment = "right",
        mouse_down = photoOther and FrontendUtil.showOtherPhotoInNavigator or nil,
        font = PluginProperties.HeaderFont,
        text_color = PluginProperties.HeaderColor,
      },
    }
  })
end

local function appendBlockHeadlineToElementList(_delta, _block, _columnContent)
  local f = LrView.osFactory()
  table.insert(_columnContent, f:row {
    margin_top = 17,
    f:static_text {
      title = TranslationUtil.translateGeneric(_block.title),
      font = PluginProperties.HeadlineFont,
      text_color = PluginProperties.HeadlineColor,
    },
  })
end

local function conditionallyInsertSummary(_subBlock, _groupBoxContent)
  if (PrefsUtil.isShowSummaryEnabled() and _subBlock.summary) then
    local f = LrView.osFactory()
    table.insert(_groupBoxContent, f:row {
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.title),
              width = LrView.share(col1sync),
              fill_horizontal = PluginProperties.ColumnWeight[1],
            },
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.changed),
              width = LrView.share(col2sync),
              fill_horizontal = PluginProperties.ColumnWeight[2],
              alignment = "right",
            },
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.unchanged),
              width = LrView.share(col3sync),
              fill_horizontal = PluginProperties.ColumnWeight[3],
              alignment = "right",
            },
        }
    )
    return true
  end
  return false
end


local function appendBlockToElementList(_delta, _block, _columnContent)
  local f = LrView.osFactory()
  for _,subBlock in ipairs(_block) do
    local groupBoxContent = {}
    for _,v in ipairs(subBlock) do
      if (_delta[v] ~= nil) then
        if (conditionallyInsertSummary(subBlock, groupBoxContent)) then
          break
        else
          table.insert(groupBoxContent, f:row {
                  f:static_text {
                    title = TranslationUtil.translateEnumeratedSetting(v, _block.title),
                    width = LrView.share(col1sync),
                    fill_horizontal = PluginProperties.ColumnWeight[1],
                  },
                  f:static_text {
                    title = nil == _delta[v][1] and "" or _delta[v][1],
                    width = LrView.share(col2sync),
                    fill_horizontal = PluginProperties.ColumnWeight[2],
                    alignment = "right",
                  },
                  f:static_text {
                    title = nil == _delta[v][2] and "" or _delta[v][2],
                    width = LrView.share(col3sync),
                    fill_horizontal = PluginProperties.ColumnWeight[3],
                    alignment = "right",
                  },
              }
          )
        end
      end
    end
    if (0 ~= #groupBoxContent) then
      table.insert(_columnContent, f:group_box {
        title = subBlock.title and TranslationUtil.translateGeneric(subBlock.title) or nil,
        show_title = subBlock.title,
        spacing = 0,
        fill_horizontal = 1,
        unpack(groupBoxContent)
      })
    end
  end
end

--[[
  Uses the block definition tables to form
  an output that matches in it's structure
  the lightroom panels on the right.
]]
function FrontendUtil.formOutputTable(_delta, _blocks)
  local f = LrView.osFactory()
  local columnContent = {}
  local renderableContent = false

  appendTableHeadToElementList(f, columnContent)
  -- if called with two params, structure in blocks
  for _,block in ipairs(_blocks) do
    if(blockMustBeRendered(_delta, block)) then
      renderableContent = true
      appendBlockHeadlineToElementList(_delta, block, columnContent)
      appendBlockToElementList(_delta, block, columnContent)
    end
  end

  if (renderableContent) then
    return f:column{
      spacing = 5,
      unpack(columnContent)
    }
  else
    return f:column{
      f:row{
        f:static_text{
          title = LOC"$$$/lidiplu/Message/NoDelta=No differences found.",
        },
      }
    }
  end
end

--[[
  Uses the block definition tables to form
  a tabbed pane that matches in it's structure
  the lightroom panels on the right.
]]
function FrontendUtil.formTabbedPane(_delta, _blocks, _tabCount)
  local f = LrView.osFactory()
  local tabs = {}

  for blockNo,block in ipairs(_blocks) do
    if(blockMustBeRendered(_delta, block)) then
      local title = TranslationUtil.translateGeneric(block.title)
      local columnContent = {}

      appendTableHeadToElementList(f, columnContent)
      appendBlockToElementList(_delta, block, columnContent)

      table.insert(tabs, f:tab_view_item {
        title = title,
        identifier = "tabNo." .. blockNo,
        fill_horizontal = 1,
        f:column {
          width = WIN_ENV and (PluginProperties.TabWidthWin * _tabCount) or (PluginProperties.TabWidthMac * _tabCount),
          spacing = 8,
          unpack (columnContent),
        }
      })
    end
  end

  if (0 == #tabs) then
    return f:column {
      f:row {
        f:static_text {
          title = LOC "$$$/lidiplu/Message/NoDelta=No differences found.",
        },
      }
    }
  else
    return f:tab_view {
      unpack(tabs)
    }
  end
end


function FrontendUtil.openInDefaultApp(file)
  if WIN_ENV then
    LrShell.openFilesInApp ({""}, file)
  else
    LrShell.openFilesInApp ({file}, "open")
  end
end

return FrontendUtil

