--global
local LrApplication = import 'LrApplication'
local LrTasks = import 'LrTasks'
local LrDialogs = import 'LrDialogs'
local LrLogger = import 'LrLogger'

--local
local BackendUtil = require 'BackendUtil'
local FrontendUtil = require 'FrontendUtil'
local PluginProperties = require 'PluginProperties'
local PrefsUtil = require 'PrefsUtil'
local DevelopmentBlocks = require 'DevelopmentBlocks'

-- configure Logging
-- todo: externalize logging to module and examine configuration options
local Log = LrLogger('LrDevSettingsCompareLogger')
Log:enable("print")


-- for debugging purposes (stolen from 'lua gems')
-- todo: refactor to separate namespace and pass the output function
local function vardump (value, depth, key)
  local linePrefix = ""
  local spaces = ""

  if key ~= nil then
    linePrefix = "["..key.."] = "
  end

  if depth == nil then
    depth = 0
  else
    depth = depth + 1
    for i=1, depth do spaces = spaces .. "  " end
  end

  if type(value) == 'table' then
    mTable = getmetatable(value)
    if mTable == nil then
      Log:trace(spaces ..linePrefix.."(table) ")
    else
      Log:trace(spaces .."(metatable) ")
        value = mTable
    end
    for tableKey, tableValue in pairs(value) do
      vardump(tableValue, depth, tableKey)
    end
  elseif type(value)	== 'function' or
      type(value)	== 'thread' or
      type(value)	== 'userdata' or
      value		== nil
  then
    Log:trace(spaces..tostring(value))
  else
    Log:trace(spaces..linePrefix.."("..type(value)..") "..tostring(value))
  end
end

-- do comparison
local function compareDevSettings(devSettingsActive, devSettingsOther)
  local threshold = PrefsUtil.getThreshold()
  local debugMode = PrefsUtil.isDebugModeEnabled()

  -- iterate devsettings of first photo, compare with second, save delta in table
  local deltaBack = BackendUtil.flattenedDelta(devSettingsActive, devSettingsOther)

  -- iterate devsettings of second photo, compare with first, use the same table to save
  local deltaBackForth = BackendUtil.flattenedDelta(devSettingsOther, devSettingsActive, deltaBack, nil, true)

  local panel = nil
  local count = BackendUtil.countRelevantEntries(deltaBackForth, DevelopmentBlocks)

  -- decides if a compact table or a tabbedPane will be rendered
  if (count <= threshold) then
    panel = FrontendUtil.formOutputTable(
      deltaBackForth
      ,DevelopmentBlocks
    )
  else
    panel = FrontendUtil.formTabbedPane(
      deltaBackForth
      ,DevelopmentBlocks
    )
  end

  LrDialogs.presentModalDialog {
          title = LOC "$$$/lidiplu/WindowTitle=DevSettingsCompare",
          contents = panel,
          resizable = true,
  }

  -- always switch back
  FrontendUtil.showActivePhotoInNavigator()

  if debugMode then
    Log:trace("==========================")
    vardump(deltaBackForth)
  end
end

-- main
local function main()
  local catalog = LrApplication.activeCatalog()
  local selectedPhotos = catalog:getMultipleSelectedOrAllPhotos()
  local targetPhoto = catalog:getTargetPhoto()

  LrTasks.startAsyncTask(function()
      if (#selectedPhotos == 2) then
        --todo: make them local again
        photoActive = selectedPhotos[1]
        photoOther = selectedPhotos[2]

        -- active photo is always the first one
        if (catalog:getTargetPhoto() == photoOther) then
          photoActive, photoOther = photoOther, photoActive
        end

        local devSettingsActive = photoActive:getDevelopSettings()
        local devSettingsOther = photoOther:getDevelopSettings()

        -- do the comparison
        compareDevSettings(devSettingsActive, devSettingsOther)

      elseif (nil ~= targetPhoto) then
        -- todo: find better way to get default preset (i.e. is the
        --       default uuid stable it's easy with developPresetByUuid),
        -- todo: Use the preset applied on import
        local presetDefault = nil -- LrApplication.developPresetByUuid('<uuid of default>')
        local presetFolders = LrApplication.developPresetFolders()
        for _, pf in ipairs(presetFolders) do
          local presets = pf:getDevelopPresets()
          for _, p in ipairs(presets) do
             if ('Default' == p:getName()) then
               presetDefault = p
               break
             end
          end
          if (nil ~= presetDefault) then
             break
          end
        end
        if (nil ~= presetDefault) then
          --todo: make them local again
          photoActive = targetPhoto
          photoOther = targetPhoto

          local devSettingsActive = photoActive:getDevelopSettings()
          local devSettingsOther = presetDefault:getSetting()

          -- crop & straighten settings are not part of a preset
          --todo: replace with a loop, i.e. from DevelopmentBlocks
          devSettingsOther["CropTop"] = devSettingsActive["CropTop"]
          devSettingsOther["CropRight"] = devSettingsActive["CropRight"]
          devSettingsOther["CropBottom"] = devSettingsActive["CropBottom"]
          devSettingsOther["CropLeft"] = devSettingsActive["CropLeft"]
          devSettingsOther["CropAngle"] = devSettingsActive["CropAngle"]
          devSettingsOther["CropConstrainToWarp"] = devSettingsActive["CropConstrainToWarp"]

          -- do the comparison
          compareDevSettings(devSettingsActive, devSettingsOther)

        else
          local m1 = LOC "$$$/lidiplu/Warning/NoDefaultPreset=No 'Default' preset found."
          local m2 = LOC "$$$/lidiplu/Warning/CreateDefaultPreset=" 
                         .. "Please create a preset with your default development settings and save it with name 'Default'."
          local m3 = LOC "$$$/lidiplu/Warning/CreateDefaultPreset2="
                         .. "Ensure that all development settings are stored in the 'Default' preset ('Check All')."
          local message = m1 .. "\n\n" .. m2 .. "\n" .. m3
          LrDialogs.message(message, nil, "critical")
        end

      else
        local message = LOC "$$$/lidiplu/Warning/SelectOneOrTwo=Please select exactly one or two photos."
        LrDialogs.message(message, nil, "critical")
      end
  end)
end


main()