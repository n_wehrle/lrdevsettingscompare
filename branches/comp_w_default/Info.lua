--[[
  Compares the development settings of
  two different photos.

  Plugin:  LrDevSettingsCompare
  Version: 20101015(SVN 75M)
  Authors: Norman Wehrle, Matthias Saft

  Done:
    - get dev settings of exacly two selected pictures
    - extract only settings that differ
    - examine which of the 2 selected fotos is the active one
      (this will be the orig in the comparison)
    - show settings side by side in a simple messagebox
    - handle tables inside tables properly
    - enable grouping by block
    - define blocks matching the LR3 Panels on the right
    - i18n
    - beautify GUI
      vertical alignment of presets with the same name

  ======================================================
  
  Todo:
    - calc delta and render:
        Clarity  |  1  |  17(+16)
    - beautify GUI more
        color coding (small change, big change, value only here)
        sliders or charts instead of bare numbers
    - statistics?
    - calc delta to dev preset
    - compare dev presets (2nd menu entry = 2nd script)
]]
return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 3.0,

	-- todo: identifier to be discussed
    LrToolkitIdentifier = 'ch.dslr.lightroom.lrdevsettingscompare',
    LrPluginName = LOC "$$$/lidiplu/PluginName=DevSettingsCompare",
    LrPluginInfoUrl = "http://sourceforge.net/projects/lidiplu/",
	
    LrPluginInfoProvider = "PluginInfoProvider.lua",

	-- only in file menu until yet
    LrExportMenuItems = {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompare=Compare Development Settings",
		file = "PluginMain.lua",
        enabledWhen = "photosSelected",
	},

	VERSION = { display="20101015(SVN 75M)" },
    LrAlsoUseBuiltInTranslations = true,
}


	