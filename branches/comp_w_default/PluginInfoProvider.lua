-- global
local LrView = import "LrView"
local LrHttp = import "LrHttp"
local LrBinding = import "LrBinding"
local LrPrefs = import "LrPrefs"

-- local
local PluginProperties = require "PluginProperties"
local PrefsUtil = require 'PrefsUtil'

local function sectionsForTopOfDialog(f, _)
  local props = LrPrefs.prefsForPlugin()

  -- if more differences found than configured, show in tabs instead of big table
  props.threshold = PrefsUtil.getThreshold()
  props.debugMode = PrefsUtil.isDebugModeEnabled()
  props.showSummary = PrefsUtil.isShowSummaryEnabled()

  return {
      {
      title = LOC "$$$/lidiplu/Config/Headline=Configuration",
      place = "horizontal",

        f:column {
          bind_to_object = props,
          spacing = f:control_spacing(),
          fill_horizontal = 1,
          f:row {
            f:checkbox{
              title = LOC "$$$/lidiplu/Config/Option/DebugMode=Enable Debug Mode",
              value = LrView.bind("debugMode"),
            },
          },
          f:row {
            f:checkbox{
              title = LOC "$$$/lidiplu/Config/Option/ShowSummary=Summarize tonecurve changes and retouchings",
              value = LrView.bind("showSummary"),
            },
          },
          f:group_box{
            place = "horizontal",
            f:static_text{
              title = LOC "$$$/lidiplu/Config/Option/Threshold=Differences needed to switch to tabbed GUI",
            },
            f:slider{
              value = LrView.bind("threshold"),
              min = 0,
              max = 50,
              integral = true,
            },
            f:edit_field{
              value = LrView.bind("threshold"),
              min = 0,
              max = 50,
              width_in_digits = 2,
              increment = 1,
              large_increment = 5,
              precision = 0,
            },
          }
        },
        f:column {
          f:push_button{
            width = 150,
            title = LOC "$$$/lidiplu/Config/Action/Bugreport=Report Bug",
            enabled = true,
            action = function()
              LrHttp.openUrlInBrowser(PluginProperties.UrlForBugReport)
              end,
          },
        }
    },
  }
end

return {
  sectionsForTopOfDialog = sectionsForTopOfDialog,
}