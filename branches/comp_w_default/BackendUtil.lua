local PrefsUtil = require 'PrefsUtil'

local BackendUtil = {}
--[[
  Takes two tables, returns a table with just the differences.

  Implementation:
    Needs to be called twice because we cannot be sure that both
    tables have the same size and use the same keys!
]]
function BackendUtil.flattenedDelta(_tab1, _tab2, _delta, _prefix, _saveReversed)
  local _delta = _delta or {}
  local _prefix = _prefix or ""
  local _saveReversed = _saveReversed or false

  for k,v in pairs(_tab1) do
    local v2 = _tab2[k]
    if (v ~= v2) then
      if ("table" == type(v) and "table" == type(v2)) then
        BackendUtil.flattenedDelta(v, v2, _delta, _prefix .. k .. ".", _saveReversed)

      -- I don't expect this to happen in real life
      -- todo: log warning
      elseif ("table" == type(v)) then
        if (nil ~= v2) then
          _delta[_prefix .. k] = _saveReversed and {v2} or {nil, v2}
        end
        BackendUtil.flattenedDelta(v, {}, _delta, _prefix .. k .. ".", _saveReversed)

      -- I don't expect this to happen in real life
      -- todo: log warning
      elseif ("table" == type(v2)) then
        if (nil ~= v) then
          _delta[_prefix .. k] = _saveReversed and {nil, v} or {v}
        end
        BackendUtil.flattenedDelta(v2, {}, _delta, _prefix .. k .. ".", not _saveReversed)

      else
        _delta[_prefix .. k] = _saveReversed and {v2, v} or {v, v2}
      end
    end
  end

  return _delta
end

-- count relevant devsettings
-- does not count lines (for simplicity)
function BackendUtil.countRelevantEntries(_delta, _blocks)
  local devSettingCount = 0
  for _,block in ipairs(_blocks) do
    for _,subBlock in ipairs(block) do
      for _,v in ipairs(subBlock) do
        if (_delta[v] ~= nil) then
          devSettingCount = devSettingCount + 1
          -- do not count any occurence of a setting if this subblock's are summarized 
          if (PrefsUtil.isShowSummaryEnabled() and subBlock.summary) then
            break
          end
        end
      end
    end
  end
  return devSettingCount
end


return BackendUtil

