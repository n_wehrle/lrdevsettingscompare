--global
local LrLocalization = import 'LrLocalization'

--local
local PluginProperties = require 'PluginProperties'
local PrefsUtil = require 'PrefsUtil'

local TranslationUtil = {
  undefined = "__undef__",
}


function TranslationUtil.splitOnFirstPeriod(input)
  local index = string.find(input, "%.")
  if (index and index > 1) then
    return (string.sub(input, 1, index - 1)),
           (string.sub(input, index))
  else
    return input
  end
end


function TranslationUtil.lastWord(input)
  local result
  for part in string.gmatch(input, "[%a&]+") do
    result = part
  end
  return result
end


function TranslationUtil.translateMeta(meta)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or meta
  -- hack for locale en
  if "en" == LrLocalization.currentLanguage() then
    return LOC(string.format("$$$/lidiplu/Meta/%s=%s", meta, defaultVal))
  end
  -- end hack
  return LOC(string.format("$$$/AgLibrary/MetadataFormatters/%s=%s", meta, defaultVal))
end


function TranslationUtil.translateGeneric(zString)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or TranslationUtil.lastWord(zString)
  return LOC(string.format("%s=%s", zString, defaultVal))
end


-- todo: remove method and use generic method above if en-hack not necessary anymore 
function TranslationUtil.translateTitle(title)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or TranslationUtil.lastWord(title)

  --todo: return defaultVal instead of undef if en-hack is not needed anymore
  local localizedString = LOC(string.format("%s=%s", title, TranslationUtil.undefined))
  -- hack for locale en
  if (TranslationUtil.undefined == localizedString) then
    localizedString = LOC(string.format("$$$/lidiplu/DevPanel/Labels/%s=%s", 
                                        TranslationUtil.lastWord(title), defaultVal))
  end
  --end hack

  return localizedString
end


function TranslationUtil.translateSetting(setting, blocktitle)
  local defaultVal = PrefsUtil.isDebugModeEnabled() and TranslationUtil.undefined or setting

  local patterns = {
    "$$$/AgCameraRawUI/%s=%s",
    "$$$/AgCameraRawNamedSettings/CameraRawSettingMapping/%s=%s",
    "$$$/AgCameraRawNamedSettings/CameraRawSettingMapping/%sCalibration=%s",
    "$$$/AgDevelop/Panel/%s=%s",
    "$$$/lidiplu/Setting/%s=%s",
  }

  local localizedString = TranslationUtil.undefined
  for _,pattern in ipairs(patterns) do
    localizedString = LOC(string.format(pattern, setting, TranslationUtil.undefined))
    if (TranslationUtil.undefined ~= localizedString) then
      break
    end
  end

  -- hack for locale en
  if (TranslationUtil.undefined == localizedString) then
    localizedString = LOC(string.format("$$$/lidiplu/DevPanel/Labels/%s/%s=%s",
                     TranslationUtil.lastWord(blocktitle), setting, TranslationUtil.undefined))
  end
  -- end hack

  -- depending on preferences return key as fallback or __undef__
  localizedString = (TranslationUtil.undefined == localizedString and defaultVal) or localizedString

  if (3 <= string.len(localizedString) and "$$$" == string.sub(localizedString, 1, 3)) then
    localizedString = LOC(string.format("%s=%s", localizedString, defaultVal))
  end
  
  return localizedString
end


-- to handle ToneCurve.27
function TranslationUtil.translateEnumeratedSetting(setting, blocktitle)
  local counter
  setting, counter = TranslationUtil.splitOnFirstPeriod(setting)
  return counter and (TranslationUtil.translateSetting(setting, blocktitle) .. counter)
    or TranslationUtil.translateSetting(setting, blocktitle)
end

return TranslationUtil