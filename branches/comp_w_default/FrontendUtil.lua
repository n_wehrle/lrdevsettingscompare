--global
local LrApplication = import 'LrApplication'
local LrView = import 'LrView'

--local
local PluginProperties = require 'PluginProperties'
local TranslationUtil = require 'TranslationUtil'
local PrefsUtil = require 'PrefsUtil'

local FrontendUtil = {}

local function subBlockMustBeRendered(_delta, _subBlock)
  for _,v in ipairs(_subBlock) do
    if (_delta[v] ~= nil) then
      return true
    end
  end
  return false
end

local function blockMustBeRendered(_delta, _block)
  for _,subBlock in ipairs(_block) do
    if (subBlockMustBeRendered(_delta, subBlock)) then
      return true
    end
  end
  return false
end

function FrontendUtil.showActivePhotoInNavigator(target)
  local catalog = LrApplication.activeCatalog()
  catalog:setSelectedPhotos( photoActive, {photoOther})
end

function FrontendUtil.showOtherPhotoInNavigator(target)
  local catalog = LrApplication.activeCatalog()
  catalog:setSelectedPhotos( photoOther, {photoActive})
end

local function appendTableHeadToElementList(f, _columnContent)
  table.insert(_columnContent, f:group_box {
    spacing = 0,
    show_title = false,
    f:row {
      f:static_text {
        title = LOC "$$$/lidiplu/Selection/Selection=Selection",
        width_in_chars = PluginProperties.ColumnWidthInChars[1],
      },
      f:static_text {
        title = LOC "$$$/lidiplu/Selection/Active=Active",
        width_in_chars = PluginProperties.ColumnWidthInChars[2],
        alignment = "right",
        mouse_down = FrontendUtil.showActivePhotoInNavigator,
      },
      f:static_text {
        title = LOC "$$$/lidiplu/Selection/Other=Other",
        width_in_chars = PluginProperties.ColumnWidthInChars[3],
        alignment = "right",
        mouse_down = FrontendUtil.showOtherPhotoInNavigator,
      },
    },
    f:row {
      f:static_text {
        title = TranslationUtil.translateMeta("FileName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[1],
      },
      f:static_text {
        title = photoActive:getFormattedMetadata("fileName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[2],
        alignment = "right",
        mouse_down = FrontendUtil.showActivePhotoInNavigator,
        truncation = "head",
      },
      f:static_text {
        title = photoOther:getFormattedMetadata("fileName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[3],
        alignment = "right",
        mouse_down = FrontendUtil.showOtherPhotoInNavigator,
        truncation = "head",
      },
    },
    f:row {
      f:static_text {
        title = TranslationUtil.translateMeta("CopyName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[1],
      },
      f:static_text {
        title = photoActive:getFormattedMetadata("copyName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[2],
        alignment = "right",
        mouse_down = FrontendUtil.showActivePhotoInNavigator,
        truncation = "head",
      },
      f:static_text {
        title = photoOther:getFormattedMetadata("copyName"),
        width_in_chars = PluginProperties.ColumnWidthInChars[3],
        alignment = "right",
        mouse_down = FrontendUtil.showOtherPhotoInNavigator,
        truncation = "head",
      },
    }
  })
end

local function appendBlockHeadlineToElementList(_delta, _block, _columnContent)
  local f = LrView.osFactory()
  table.insert(_columnContent, f:row {
    margin_top = 17,
    f:static_text {
      title = TranslationUtil.translateTitle(_block.title),
    },
  })
end

local function conditionallyInsertSummary(_subBlock, _groupBoxContent)
  if (PrefsUtil.isShowSummaryEnabled() and _subBlock.summary) then
    local f = LrView.osFactory()
    table.insert(_groupBoxContent, f:row {
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.title),
              width_in_chars = PluginProperties.ColumnWidthInChars[1],
            },
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.unchanged),
              width_in_chars = PluginProperties.ColumnWidthInChars[2],
              alignment = "right",
            },
            f:static_text {
              title = TranslationUtil.translateGeneric(_subBlock.summary.changed),
              width_in_chars = PluginProperties.ColumnWidthInChars[3],
              alignment = "right",
            },
        }
    )
    return true
  end
  return false
end


local function appendBlockToElementList(_delta, _block, _columnContent)
  local f = LrView.osFactory()
  for _,subBlock in ipairs(_block) do
    local groupBoxContent = {}
    for _,v in ipairs(subBlock) do
      if (_delta[v] ~= nil) then
        if (conditionallyInsertSummary(subBlock, groupBoxContent)) then
          break
        else
          table.insert(groupBoxContent, f:row {
                  f:static_text {
                    title = TranslationUtil.translateEnumeratedSetting(v, _block.title),
                    width_in_chars = PluginProperties.ColumnWidthInChars[1],
                  },
                  f:static_text {
                    title = nil == _delta[v][1] and "" or _delta[v][1],
                    width_in_chars = PluginProperties.ColumnWidthInChars[2],
                    alignment = "right",
                  },
                  f:static_text {
                    title = nil == _delta[v][2] and "" or _delta[v][2],
                    width_in_chars = PluginProperties.ColumnWidthInChars[3],
                    alignment = "right",
                  },
              }
          )
        end
      end
    end
    if (0 ~= #groupBoxContent) then
      table.insert(_columnContent, f:group_box {
        title = subBlock.title and TranslationUtil.translateTitle(subBlock.title) or nil,
        show_title = subBlock.title,
        spacing = 0,
        unpack(groupBoxContent)
      })
    end
  end
end

--[[
  Uses the block definition tables to form
  an output that matches in it's structure
  the lightroom panels on the right.
]]
function FrontendUtil.formOutputTable(_delta, _blocks)
  local f = LrView.osFactory()
  local columnContent = {}
  local renderableContent = false

  appendTableHeadToElementList(f, columnContent)
  -- if called with two params, structure in blocks
  for _,block in ipairs(_blocks) do
    if(blockMustBeRendered(_delta, block)) then
      renderableContent = true
      appendBlockHeadlineToElementList(_delta, block, columnContent)
      appendBlockToElementList(_delta, block, columnContent)
    end
  end

  if (renderableContent) then
    return f:column{
      spacing = 8,
      unpack(columnContent)
    }
  else
    return f:column{
      f:row{
        f:static_text{
          title = LOC"$$$/lidiplu/Message/NoDelta=No differences found.",
        },
      }
    }
  end
end

--[[
  Uses the block definition tables to form
  a tabbed pane that matches in it's structure
  the lightroom panels on the right.
]]
function FrontendUtil.formTabbedPane(_delta, _blocks)
  local f = LrView.osFactory()
  local tabs = {}

  for _,block in ipairs(_blocks) do
    if(blockMustBeRendered(_delta, block)) then
      local title = TranslationUtil.translateTitle(block.title)
      local columnContent = {}

      appendTableHeadToElementList(f, columnContent)
      appendBlockToElementList(_delta, block, columnContent)

      table.insert(tabs, f:tab_view_item {
        title = title,
        identifier = title,
        f:column {
          spacing = 8,
          unpack (columnContent),
        }
      })
    end
  end

  if (0 == #tabs) then
    return f:column {
      f:row {
        f:static_text {
          title = LOC "$$$/lidiplu/Message/NoDelta=No differences found.",
        },
      }
    }
  else
    return f:tab_view {
      unpack(tabs)
    }
  end
end

return FrontendUtil

